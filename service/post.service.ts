import {Post} from "@/lib/types";
import {http} from "@/utils/http";
import {MessageFormat} from "@/utils/message-format";

const ServiceId = {
    POST: '/posts',
    GET_BY_ID: '/posts/{0}'
}




const getPosts = async (): Promise<Post[]> => {
    const result = await http.get(ServiceId.POST)
    return result?.data
}

export const postService = {
    getPosts
}