import {http} from "@/utils/http";
import {MessageFormat} from "@/utils/message-format";
import {User} from "@/lib/types";


const ServiceId = {
    USER: '/users',
    GET_BY_ID: '/users/{0}'
}

const getUsers = async (): Promise<User[]> => {
    const result = await http.get(ServiceId.USER)
    return result?.data
}

const getUserById = async (id: string ): Promise<User> => {
    const result = await http.get(MessageFormat.format(ServiceId.GET_BY_ID, id))
    return result?.data
}

export const userService = {
    getUsers,
    getUserById
}