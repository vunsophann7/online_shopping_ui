import {Product} from "@/lib/types";
import {http} from "@/utils/http";


const ServiceId = {
    PRODUCT: '/products'
}

const getProducts = async () => {
    const result = await http.get(ServiceId.PRODUCT)
    return result?.data
}

export const productService = {
    getProducts
}