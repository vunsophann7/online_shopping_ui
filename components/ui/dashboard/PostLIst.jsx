import useFetchUsers from "../../../lib/hooks/use-fetch-users";
import Link from "next/link";

const PostLIst = () => {

    const {posts} = useFetchUsers();

    return (
        <>
            <h2>Posts</h2>
            {
                posts?.map(user => (
                    <h5 key={posts.id}>
                        <Link href={'/posts/' + posts.id}>{posts.name}</Link>
                    </h5>
                ))
            }
        </>
    )

}