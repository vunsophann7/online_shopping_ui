export interface Product {
    id: string,
    name: string,
    price: string,
    description: string,
    stock: number,
    category: string

}