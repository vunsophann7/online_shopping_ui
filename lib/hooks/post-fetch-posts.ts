import {useQuery} from "@tanstack/react-query";
import {postService} from "@/service/post.service";

const useFetchPosts = () => {
    const query = useQuery({
        queryKey: ["posts"],
        queryFn: () => postService.getPosts()
    })

    return {
        isLoading: query?.isLoading,
        isError: query?.isError,
        users: query?.data ?? []
    }
}

export default useFetchPosts;