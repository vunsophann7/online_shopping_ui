import {useQuery} from "@tanstack/react-query";
import {productService} from "@/service/product.service";


const useFetchProducts = () => {
    const query = useQuery({
        queryKey: ["products"],
        queryFn: async () => await productService.getProducts()
    })

    return {
        isLoading: query?.isLoading,
        isError: query?.isError,
        products: query?.data ?? [],
    }

}

export default useFetchProducts;