import React from 'react';
import OnlineShopping from "@/components/ui/online-shopping/Online-Shopping";

const Page = () => {
    return (
        <div>
            <OnlineShopping />
        </div>
    );
};

export default Page;