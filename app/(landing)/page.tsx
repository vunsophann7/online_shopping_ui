import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import Link from "next/link";

const Page = () => {
    return (
        <div>
            <div className={"d-flex gap-2"}>
                <Link href={"/login"}>Login</Link>
                <Link href={"/signup"}>SignUp</Link>
                <Link href={"/dashboard"}>Dashboard</Link>
                <Link href={"/online-shopping"}>Online Shopping</Link>
            </div>
            <h2>Landing Page</h2>
        </div>
    );
};

export default Page;